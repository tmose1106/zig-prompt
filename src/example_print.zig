const std = @import("std");
const os = std.os;

const prompt = @import("main.zig");

pub fn main() anyerror!void {
    const stdin = std.io.getStdIn();
    const stderr = std.io.getStdErr();

    try prompt.readInputAndPrintKeyCodes(stdin, stderr);
}
