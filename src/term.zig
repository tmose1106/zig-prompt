const std = @import("std");

const fs = std.fs;
const os = std.os;

const VTIME = 5;
const VMIN = 6;

const BRKINT = 0o0002;
const INPCK = 0o0020;
const ISTRIP = 0o0040;
const ICRNL = 0o0400;
const IXON = 0o2000;

/// TODO: Use global termios or not? Makes usage nicer
var original_termios: std.os.termios = undefined;

/// Enable raw terminal mode where text is not printed when typed,
/// but rather sent to the program directly. This mode can be disabled
/// by calling tcsetattr with the termios prior to entering raw mode.
pub fn enableRawMode(file: fs.File) !void {
    var raw = try os.tcgetattr(file.handle);

    // std.mem.copy(os.termios, original_termios, raw);
    original_termios = raw;

    raw.iflag &= ~@as(@TypeOf(raw.lflag), BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    raw.oflag &= ~@as(@TypeOf(raw.lflag), os.OPOST);
    raw.cflag &= ~@as(@TypeOf(raw.lflag), os.CS8);
    raw.lflag &= ~@as(@TypeOf(raw.lflag), os.ECHO | os.ICANON | os.IEXTEN | os.ISIG);
    raw.cc[VMIN] = 1;
    raw.cc[VTIME] = 0;

    try os.tcsetattr(file.handle, os.TCSA.FLUSH, raw);
}

/// Disable raw mode using global variable of original values
pub fn disableRawMode(file: fs.File) !void {
    try os.tcsetattr(file.handle, os.TCSA.FLUSH, original_termios);
}
