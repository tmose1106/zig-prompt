const std = @import("std");

const ascii = std.ascii;
const fs = std.fs;
const os = std.os;

const Allocator = std.mem.Allocator;

const expect = std.testing.expect;

const term = @import("term.zig");

const HistoryBuffer = @import("history_buffer.zig").HistoryBuffer;

const Prompt = struct {
    const Self = @This();

    alloc: *Allocator,

    var buffer: []u8 = undefined;
    var history: HistoryBuffer(64) = undefined;

    pub fn init(self: *Self) !void {
        buffer = try self.alloc.alloc(u8, 1024);

        history = HistoryBuffer(64){ .alloc = self.alloc };
    }

    pub fn deinit(self: *Self) void {
        self.alloc.free(buffer);

        history.deinit();
    }

    /// TODO: Better to make multiple writeAll calls or allocate a
    /// concatentation and call writeAll once?
    fn refreshLine(stream: fs.File.OutStream, prompt: []const u8) !void {
        // Jump to start of line and clear entire line to the right
        try stream.writeAll("\r\x1b[0K");
        // Rewrite the prompt
        try stream.writeAll(prompt);

        try stream.writeAll(content);
    }

    pub fn readAndEdit(in: fs.File, out: fs.File, prompt: []const u8, delimiter: u8) !?[]u8 {
        try term.enableRawMode(in);

        var in_stream = in.inStream();
        var out_stream = out.outStream();

        try out_stream.writeAll(prompt);

        // Can receive a maximum of three bytes from an escape code
        var sequence: [3]u8 = undefined;
        var index: usize = 0;

        while (true) {
            const byte = try in_stream.readByte();

            switch (byte) {
                0x0d => { // RETURN
                    break;
                },
                0x15 => { // CTRL-U
                    for (buffer) |*tmp_byte| {
                        tmp_byte.* = undefined;
                    }

                    index = 0;

                    try refreshLine(out_stream, buffer, prompt);
                },
                0x1b => { // ESCAPE
                    sequence[0] = try in_stream.readByte();
                    sequence[1] = try in_stream.readByte();

                    if (sequence[0] == '[') {
                        if (std.ascii.isDigit(sequence[1])) {
                            sequence[2] = try in_stream.readByte();

                            if (sequence[2] == '~') {
                                if (sequence[1] == '3') { // DELETE
                                }
                            }
                        } else {
                            switch (sequence[1]) {
                                'A' => { // UP
                                },
                                else => continue,
                            }
                        }
                    }
                },
                0x7f => { // BACKSPACE
                    if (index <= 0) continue;

                    index -= 1;

                    buffer[index] = undefined;

                    try refreshLine(out_stream, buffer, prompt);
                },
                else => {
                    buffer[index] = byte;

                    index += 1;

                    try out_stream.writeByte(byte);
                },
            }
        }

        try term.disableRawMode(in);

        if (index == 0) {
            return null;
        }

        return buffer[0..index];
    }
};

fn getByteInQuotes(byte: u8) [3]u8 {
    return [3]u8{ '\'', byte, '\'' };
}

/// Convert ascii control code byte to its name
fn getControlCodeString(byte: u8) []const u8 {
    return switch (byte) {
        0 => "NUL",
        1 => "SOH",
        2 => "STX",
        3 => "ETX",
        4 => "EOT",
        5 => "ENQ",
        6 => "ACK",
        7 => "BEL",
        8 => "BS",
        9 => "HT",
        10 => "LF",
        11 => "VT",
        12 => "FF",
        13 => "CR",
        14 => "SO",
        15 => "SI",
        16 => "DLE",
        17 => "DC1",
        18 => "DC2",
        19 => "DC3",
        20 => "DC4",
        21 => "NAK",
        22 => "SYN",
        23 => "ETB",
        24 => "CAN",
        25 => "EM",
        26 => "SUB",
        27 => "ESC",
        28 => "FS",
        29 => "GS",
        30 => "RS",
        31 => "US",
        127 => "DEL",
        else => "",
    };
}

/// Read user input and print the associated ASCII codes. This is not
/// meant for application use, but is used by example.zig
pub fn readInputAndPrintKeyCodes(in: fs.File, out: fs.File) !void {
    const original_termios = try os.tcgetattr(in.handle);

    try term.enableRawMode(in);

    var inStream = in.inStream();
    var outStream = out.outStream();

    try outStream.print("Press keys to see scan codes. Type uppercase 'Q' to exit.\n\r", .{});

    while (true) {
        const byte = try inStream.readByte();

        const debug_string = if (std.ascii.isCntrl(byte)) getControlCodeString(byte) else &getByteInQuotes(byte);

        try outStream.print("{: <3} {x:0>2} ({d: >3})\n\r", .{ debug_string, byte, byte });

        if (byte == 81) break;
    }

    // TODO: Try using defer here, maybe? Putting in defer block says
    // that errors can't be returned from defer, thus no try
    // statements
    try os.tcsetattr(in.handle, os.TCSA.FLUSH, original_termios);
}

test "Prompt init and deinit" {
    var prompt = Prompt{ .alloc = std.testing.allocator };

    try prompt.init();
    defer prompt.deinit();
}
