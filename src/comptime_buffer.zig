const std = @import("std");

const assert = std.debug.assert;
const expect = std.testing.expect;

fn HistoryEntry(comptime width: usize) type {
    return struct {
        data: [width]u8 = undefined,
        len: usize = 0,
    };
}

pub fn HistoryBuffer(comptime length: usize, comptime width: usize) type {
    const Entry = HistoryEntry(width);

    return struct {
        const Self = @This();

        index: usize = 0,
        is_full: bool = false,
        entries: [length]Entry = undefined,

        pub fn push(self: *Self, str: []const u8) void {
            var entry = &self.entries[self.index];

            std.mem.copy(u8, &entry.data, str);

            entry.len = str.len;

            const index_inc = self.index + 1;

            if (index_inc == length) self.is_full = true;

            self.index = if (index_inc < length) index_inc else 0;
        }

        pub fn get(self: *Self, index: usize) []u8 {
            assert(index >= 0 and index < length);

            if (self.is_full and self.index != 0) self.rotateEntries();

            const entry = &self.entries[index];

            return entry.data[0..entry.len];
        }

        pub fn length(self: *Self) usize {
            return if (self.is_full) length else self.index;
        }

        fn getPopulatedEntries(self: *Self) [][]u8 {
            return if (self.is_full) self.entries[0..] else self.entries[0..self.index];
        }

        fn rotateEntries(self: *Self) void {
            std.mem.rotate(Entry, &self.entries, self.index);

            self.index = 0;
        }
    };
}

test "history buffer push" {
    var history = HistoryBuffer(1, 8){};

    try history.push("Hello");

    const entry = &history.entries[0];

    expect(std.mem.eql(u8, entry.data[0..entry.len], "Hello"));
}

test "history buffer push with overflow" {
    var history = HistoryBuffer(2, 8){};

    try history.push("Hello");
    try history.push("World");

    expect(history.is_full);

    try history.push("Goodbye");

    const first_entry = &history.entries[0];

    expect(std.mem.eql(u8, first_entry.data[0..first_entry.len], "Goodbye"));

    const second_entry = &history.entries[1];

    expect(std.mem.eql(u8, second_entry.data[0..second_entry.len], "World"));
}

test "history buffer get" {
    var history = HistoryBuffer(1, 8){};

    try history.push("Hello");

    const entry = history.entries[0];

    expect(std.mem.eql(u8, history.get(0), "Hello"));
}

test "history buffer get after overflow" {
    var history = HistoryBuffer(2, 8){};

    try history.push("Hello");
    try history.push("World");
    try history.push("Goodbye");

    expect(std.mem.eql(u8, history.get(0), "World"));
    expect(std.mem.eql(u8, history.get(1), "Goodbye"));
}

test "history buffer length" {
    var history = HistoryBuffer(2, 8){};

    expect(history.length() == 0);

    try history.push("Hello");

    expect(history.length() == 1);

    try history.push("World");

    expect(history.length() == 2);

    try history.push("Goodbye");

    expect(history.length() == 2);
}
