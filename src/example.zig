const std = @import("std");

const os = std.os;

const Prompt = @import("comptime_prompt.zig").Prompt;

pub fn main() anyerror!void {
    const stdin = std.io.getStdIn();
    const stderr = std.io.getStdErr();

    var prompt = Prompt(32){};

    while (true) {
        const opt_input = try prompt.readAndEdit(stdin, stderr, "echo> ", '\n');

        if (opt_input) |input| {
            prompt.addHistory(input);

            std.debug.print("\n{}\n", .{input});
        } else {
            std.debug.print("\n", .{});
            break;
        }
    }
}
