const std = @import("std");
const fs = std.fs;

const term = @import("term.zig");

const HistoryBuffer = @import("comptime_buffer.zig").HistoryBuffer;

fn clearBuffer(buffer: []u8) void {
    for (buffer) |*tmp_byte| {
        tmp_byte.* = undefined;
    }
}

fn refreshLine(stream: fs.File.OutStream, prompt: []const u8, content: []const u8) !void {
    // Jump to start of line and clear entire line to the right
    try stream.writeAll("\r\x1b[0K");
    // Rewrite the prompt
    try stream.writeAll(prompt);

    try stream.writeAll(content);
}

pub fn Prompt(comptime buf_width: usize) type {
    return struct {
        const Self = @This();

        var buffer: [buf_width]u8 = undefined;

        var history = HistoryBuffer(64, buf_width){};

        pub fn readAndEdit(self: *Self, in: fs.File, out: fs.File, prompt: []const u8, delimiter: u8) !?[]u8 {
            try term.enableRawMode(in);

            var in_stream = in.inStream();
            var out_stream = out.outStream();

            // Can receive a maximum of three bytes from an escape code
            var sequence: [3]u8 = undefined;

            var index: usize = 0;

            var history_length = history.length();
            var history_index = history_length;

            try out_stream.writeAll(prompt);
            // try out_stream.print("{} {}", .{ history_length, prompt });

            while (true) {
                const byte = try in_stream.readByte();

                switch (byte) {
                    0x04 => {
                        if (index == 0) break;
                    },
                    0x0d => { // RETURN
                        break;
                    },
                    0x15 => { // CTRL-U
                        clearBuffer(&buffer);

                        index = 0;

                        try refreshLine(out_stream, prompt, buffer[0..index]);
                    },
                    0x1b => { // ESCAPE
                        sequence[0] = try in_stream.readByte();
                        sequence[1] = try in_stream.readByte();

                        if (sequence[0] == '[') {
                            if (std.ascii.isDigit(sequence[1])) {
                                sequence[2] = try in_stream.readByte();

                                if (sequence[2] == '~') {
                                    if (sequence[1] == '3') { // DELETE
                                    }
                                }
                            } else {
                                switch (sequence[1]) {
                                    'A' => { // UP
                                        if (history_index <= 0) continue;

                                        history_index -= 1;

                                        const history_value = history.get(history_index);

                                        clearBuffer(&buffer);

                                        std.mem.copy(u8, &buffer, history_value);

                                        index = history_value.len;

                                        try refreshLine(out_stream, prompt, buffer[0..index]);
                                    },
                                    'B' => {
                                        if (history_index >= history_length) continue;

                                        history_index += 1;

                                        clearBuffer(&buffer);

                                        if (history_index != history_length) {
                                            const history_value = history.get(history_index);

                                            std.mem.copy(u8, &buffer, history_value);

                                            index = history_value.len;
                                        } else {
                                            index = 0;
                                        }

                                        try refreshLine(out_stream, prompt, buffer[0..index]);
                                    },
                                    else => continue,
                                }
                            }
                        }
                    },
                    0x7f => { // BACKSPACE
                        if (index <= 0) continue;

                        index -= 1;

                        buffer[index] = undefined;

                        try refreshLine(out_stream, prompt, buffer[0..index]);
                    },
                    else => {
                        buffer[index] = byte;

                        index += 1;

                        try out_stream.writeByte(byte);
                    },
                }
            }

            try term.disableRawMode(in);

            if (index == 0) {
                return null;
            }

            return buffer[0..index];
        }

        pub fn addHistory(self: *Self, entry: []const u8) void {
            history.push(entry);
        }

        pub fn loadHistory(path: []const u8) !void {}

        pub fn saveHistory(path: []const u8) !void {}
    };
}
