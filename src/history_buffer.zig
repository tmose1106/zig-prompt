const std = @import("std");

const Allocator = std.mem.Allocator;
const assert = std.debug.assert;
const expect = std.testing.expect;

pub fn HistoryBuffer(comptime length: usize) type {
    return struct {
        const Self = @This();

        index: usize = 0,
        is_full: bool = false,
        entries: [length][]u8 = undefined,
        alloc: *Allocator,

        pub fn deinit(self: *Self) void {
            const populated_entries = self.getPopulatedEntries();

            for (populated_entries) |entry| {
                self.alloc.free(entry);
            }
        }

        pub fn push(self: *Self, str: []const u8) !void {
            var string = try self.alloc.alloc(u8, str.len);

            std.mem.copy(u8, &string, str);

            if (self.is_full) self.alloc.free(self.entries[self.index]);

            self.entries[self.index] = string;

            const index_inc = self.index + 1;

            if (index_inc == length) self.is_full = true;

            self.index = if (index_inc < length) index_inc else 0;
        }

        pub fn get(self: *Self, index: usize) []const u8 {
            assert(index < length);

            if (self.is_full and self.index != 0) self.rotateEntries();

            return self.entries[index];
        }

        pub fn length(self: *Self) usize {
            return if (self.is_full) length else self.index;
        }

        fn getPopulatedEntries(self: *Self) [][]u8 {
            return if (self.is_full) self.entries[0..] else self.entries[0..self.index];
        }

        fn rotateEntries(self: *Self) void {
            std.mem.rotate([]u8, &self.entries, self.index);

            self.index = 0;
        }
    };
}

test "history buffer init and deinit" {
    var history = HistoryBuffer(4){ .alloc = std.testing.allocator };
    defer history.deinit();
}

test "history buffer push" {
    var history = HistoryBuffer(1){ .alloc = std.testing.allocator };
    defer history.deinit();

    try history.push("Hello");

    expect(std.mem.eql(u8, history.entries[0], "Hello"));
}

test "history buffer push with overflow" {
    var history = HistoryBuffer(2){ .alloc = std.testing.allocator };
    defer history.deinit();

    try history.push("Hello");
    try history.push("World");

    expect(history.is_full);

    try history.push("Goodbye");

    expect(std.mem.eql(u8, history.entries[0], "Goodbye"));
    expect(std.mem.eql(u8, history.entries[1], "World"));
}

test "history buffer get" {
    var history = HistoryBuffer(2){ .alloc = std.testing.allocator };
    defer history.deinit();

    try history.push("Hello");

    expect(std.mem.eql(u8, history.get(0), "Hello"));
}

test "history buffer get after overflow" {
    var history = HistoryBuffer(2){ .alloc = std.testing.allocator };
    defer history.deinit();

    try history.push("Hello");
    try history.push("World");
    try history.push("Goodbye");

    expect(std.mem.eql(u8, history.get(0), "World"));
    expect(std.mem.eql(u8, history.get(1), "Goodbye"));
}

test "history buffer length" {
    var history = HistoryBuffer(2){ .alloc = std.testing.allocator };
    defer history.deinit();

    expect(history.length() == 0);

    try history.push("Hello");

    expect(history.length() == 1);

    try history.push("World");

    expect(history.length() == 2);

    try history.push("Goodbye");

    expect(history.length() == 2);
}
