const Builder = @import("std").build.Builder;

pub fn build(b: *Builder) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const lib = b.addStaticLibrary("zig_prompt", "src/main.zig");
    lib.setBuildMode(mode);
    lib.install();

    const exe = b.addExecutable("zig-prompt-example", "src/example.zig");
    exe.setTarget(target);
    exe.setBuildMode(mode);
    exe.install();

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("example", "Run the example app");
    run_step.dependOn(&run_cmd.step);

    const example_print_exe = b.addExecutable("zig-prompt-print", "src/example_print.zig");
    example_print_exe.setTarget(target);
    example_print_exe.setBuildMode(mode);
    example_print_exe.install();

    const run_print_cmd = example_print_exe.run();
    run_print_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_print_cmd.addArgs(args);
    }

    const run_print_step = b.step("example_print", "Run the debug print example app");
    run_print_step.dependOn(&run_print_cmd.step);
}
